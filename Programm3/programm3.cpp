#include <iostream>
using namespace std;

int main() {
	int a, b;
	
	cout << "Enter size of array: ";
	cin >> a >> b;

	int **array = new int *[a];
	for (int count = 0; count < a; ++count)
		array[count] = new int[b];
	
	for (int countI = 0; countI < a; ++countI) {
		for (int countJ = 0; countJ < b; ++countJ)
			array[countI][countJ] = 1 + rand() % 10;
	}

	for (int countI = 0; countI < a; ++countI) {
		cout << "\n";
		for (int countJ = 0; countJ < b; ++countJ)
			cout << array[countI][countJ] << " ";
	}
	cout << "\n";

	for (int count = 0; count < a; count++)
		delete[] array[count];
	delete[] array;

	system("pause");
	return 0;
}